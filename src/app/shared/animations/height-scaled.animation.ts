import {animate, state, style, transition, trigger} from '@angular/animations';

export const heightScaledStateTrigger = trigger('height-scaled', [
  state('false', style({height: '0px', overflow: 'hidden'})),
  state('true', style({height: '*', overflow: 'visible'})),
  transition('* => true', [
      style({ height: '0px', overflow: 'hidden' }),
      animate('250ms ease-out', style({ height: '*' })),
    ]
  ),
  transition('* => false', [
      style({ height: '*', overflow: 'hidden' }),
      animate('250ms ease-out', style({ height: '0px' })),
    ]
  )
]);

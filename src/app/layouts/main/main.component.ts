import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  host: {
    class: 'MainRoot'
  },
})
export class MainComponent implements OnInit {

  form = new FormGroup({
    winners: new FormControl('')
  });

  constructor() { }

  ngOnInit() {
  }

}

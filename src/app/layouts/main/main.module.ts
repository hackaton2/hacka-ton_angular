import { NgModule } from '@angular/core';
import { MainComponent } from './main.component';
import { CommonModule } from '@angular/common';
import { HelpersModule } from '../../helpers/helpers.module';
import { MainHeaderComponent } from '../../components/main-header/main-header.component';
import { FooterComponent } from '../../components/footer/footer.component';
import { TimerModule } from '../../components/timer/timer.module';
import {WinnersModule} from '../../components/winners/winners.module';
import {MarketingComponent} from '../../components/marketing/marketing.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {WinnersListComponent} from '../../forms/winners-list/winners-list.component';
import {NgxUsefulSwiperModule} from 'ngx-useful-swiper';

@NgModule({
  declarations: [
    MainComponent,
    MainHeaderComponent,
    MarketingComponent,
    FooterComponent,
    WinnersListComponent
  ],
  imports: [
    CommonModule,
    TimerModule,
    WinnersModule,
    HelpersModule,
    FormsModule,
    NgxUsefulSwiperModule,
    ReactiveFormsModule
  ],
  exports: [
    MainComponent,
    MainHeaderComponent,
    MarketingComponent,
    FooterComponent,
    WinnersListComponent
  ]
})
export class MainModule {}

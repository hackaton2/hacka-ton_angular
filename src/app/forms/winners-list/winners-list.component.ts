import {Component, forwardRef, HostBinding, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {coerceBooleanProperty} from '@angular/cdk/coercion';
import {SwiperOptions} from 'swiper';
import {heightScaledStateTrigger} from '../../shared/animations/height-scaled.animation';

export interface Winner {
  name: string;
  lastNumberCard: string | number;
}

@Component({
  selector: 'app-winners-list',
  templateUrl: './winners-list.component.html',
  styleUrls: ['./winners-list.component.scss'],
  animations: [heightScaledStateTrigger],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => WinnersListComponent),
    multi: true
  }]
})
export class WinnersListComponent implements ControlValueAccessor {
  @HostBinding('class.open') get classOpen() {
    return this._open;
  }

  config: SwiperOptions = {
    navigation: {
      nextEl: '.Buttons-ArrowRight',
      prevEl: '.Buttons-ArrowLeft'
    },
    loop: true,
    spaceBetween: 30
  };

  constructor() { }

  get open() {
    return this._open;
  }
  set open(val) {
    this._open = coerceBooleanProperty(val);
    this.propagateTouched(val);
  }

  get currentWinner() {
    return this._currentWinner;
  }
  set currentWinner(winner: Winner) {
    this._currentWinner = winner;
    this.propagateChange(winner.name);
    this.propagateTouched(winner.name);
  }

  _open = false;
  _currentWinner: Winner;

  expand = false;
  i = 0;

  winnersConst: Winner[] =
  [
    {name: 'Гений П.', lastNumberCard: '1512'},
    {name: 'Евгений П.', lastNumberCard: '1843'},
    {name: 'Петро П.', lastNumberCard: '9478'},
    {name: 'Пинки П.', lastNumberCard: '3856'},
    {name: 'Брэйн П.', lastNumberCard: '5468'},
    {name: 'Конь П.', lastNumberCard: '4795'},
    {name: 'Обезьяна П.', lastNumberCard: '3589'},
    {name: 'Яна П.', lastNumberCard: '9876'},
    {name: 'Фантазер П.', lastNumberCard: '3427'},
    {name: 'Григорий П.', lastNumberCard: '2357'},
    {name: 'Тимати П.', lastNumberCard: '3567'},
    {name: 'Юрий П.', lastNumberCard: '8746'},
    {name: 'Сергей П.', lastNumberCard: '8659'},
    {name: 'Матвей П.', lastNumberCard: '4837'},
    {name: 'Тимофей П.', lastNumberCard: '3784'},
    {name: 'Елисей П.', lastNumberCard: '6734'},
    {name: 'Гардей П.', lastNumberCard: '2632'},
    {name: 'Алексей П.', lastNumberCard: '4844'},
    {name: 'Ян П.', lastNumberCard: '3324'},
  ];

  propagateChange = (winner) => {};
  propagateTouched = (winner) => {};

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.propagateTouched = fn;
  }

  writeValue(winner: Winner): void {
    this.currentWinner = winner;
  }

  onClickControl() {
    this.open = !this.open;
    this.expand = !this.expand;
  }

  toggleRight() {
    if (this.i < 18) {
      this.i += 1;
      this.currentWinner = this.winnersConst[this.i];
    } else {
      this.i = 0;
      this.currentWinner = this.winnersConst[0];
    }
  }

  toggleLeft() {
    if (this.i >= 0) {
      this.i += -1;
      this.currentWinner = this.winnersConst[this.i];
    } else {
      this.i = 18;
      this.currentWinner = this.winnersConst[0];
    }
  }

}

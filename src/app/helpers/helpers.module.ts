import {NgModule} from '@angular/core';
import {LineSeparatorComponent} from './line-separator/line-separator.component';

@NgModule({
  declarations: [
    LineSeparatorComponent,
  ],
  imports: [],
  exports: [
    LineSeparatorComponent,
  ]
})
export class HelpersModule {}

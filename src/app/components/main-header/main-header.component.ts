import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.scss'],
  host: {
    class: 'MainHeader content'
  },
  encapsulation: ViewEncapsulation.None
})
export class MainHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

import {NgModule} from '@angular/core';
import {HelpersModule} from '../../helpers/helpers.module';
import {NgxTimerModule} from 'ngx-timer';
import {TimerComponent} from './timer.component';


@NgModule({
  declarations: [
    TimerComponent],
  imports: [
    HelpersModule,
    NgxTimerModule
  ],
  exports: [
    TimerComponent]
})
export class TimerModule {}

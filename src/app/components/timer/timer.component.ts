import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import { countDownTimerConfigModel, CountdownTimerService } from 'ngx-timer';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'BlockClock'
  },
})
export class TimerComponent implements OnInit {

  constructor(
    public countdownTimerService: CountdownTimerService,
  ) {}

  config: countDownTimerConfigModel;
  day: number | string;
  month: number;

  ngOnInit() {
    const cdate = new Date();
    this.day = (31 - cdate.getDate());
    this.month = cdate.getMonth();
    cdate.setDate(cdate.getDate() + this.day);
    if (this.month < cdate.getMonth()) {
      this.day = this.day - cdate.getDate();
    }
    this.day = this.day.toString();
    if (this.day.length < 2) {
      this.day = '0' + this.day;
    }
    cdate.setHours(cdate.getHours() + (24 - cdate.getHours()));
    cdate.setMinutes(cdate.getMinutes() + (60 - cdate.getMinutes()));
    cdate.setSeconds(cdate.getSeconds() + (60 - cdate.getSeconds()));
    this.countdownTimerService.startTimer(cdate);
    this.config = new countDownTimerConfigModel();
  }

}

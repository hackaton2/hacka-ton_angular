import {NgModule} from '@angular/core';
import {WinnersComponent} from './winners.component';
import {CommonModule} from '@angular/common';
import {NgxUsefulSwiperModule} from 'ngx-useful-swiper';

@NgModule({
  declarations: [
    WinnersComponent],
    imports: [
        CommonModule,
        NgxUsefulSwiperModule,
    ],
  exports: [
    WinnersComponent]
})
export class WinnersModule {}

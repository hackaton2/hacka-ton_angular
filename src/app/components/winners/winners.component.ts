import {Component, EventEmitter, HostBinding, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {SwiperOptions} from 'swiper';
import {coerceBooleanProperty} from '@angular/cdk/coercion';
import {heightScaledStateTrigger} from '../../shared/animations/height-scaled.animation';

@Component({
  selector: 'app-winners',
  templateUrl: './winners.component.html',
  styleUrls: ['./winners.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [heightScaledStateTrigger]
})
export class WinnersComponent implements OnInit {
  @HostBinding('class.open') get classOpen() {
    return this._open;
  }

  config: SwiperOptions = {
    navigation: {
      nextEl: '.Buttons-ArrowRight',
      prevEl: '.Buttons-ArrowLeft'
    },
    loop: true,
    spaceBetween: 30
  };

  constructor() { }

  @Input()
  get open() {
    return this._open;
  }
  set open(val) {
    this._open = coerceBooleanProperty(val);
  }

  _open = false;

  @Output() openChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() openLabel: string;
  @Input() closeLabel: string;

  expand = false;
  winnersConst = [
    {name: 'Гений П.', lastNumberCard: '1512'},
    {name: 'Евгений П.', lastNumberCard: '1843'},
    {name: 'Петро П.', lastNumberCard: '9478'},
    {name: 'Пинки П.', lastNumberCard: '3856'},
    {name: 'Брэйн П.', lastNumberCard: '5468'},
    {name: 'Конь П.', lastNumberCard: '4795'},
    {name: 'Обезьяна П.', lastNumberCard: '3589'},
    {name: 'Яна П.', lastNumberCard: '9876'},
    {name: 'Фантазер П.', lastNumberCard: '3427'},
    {name: 'Григорий П.', lastNumberCard: '2357'},
    {name: 'Тимати П.', lastNumberCard: '3567'},
    {name: 'Юрий П.', lastNumberCard: '8746'},
    {name: 'Сергей П.', lastNumberCard: '8659'},
    {name: 'Матвей П.', lastNumberCard: '4837'},
    {name: 'Тимофей П.', lastNumberCard: '3784'},
    {name: 'Елисей П.', lastNumberCard: '6734'},
    {name: 'Гардей П.', lastNumberCard: '2632'},
    {name: 'Алексей П.', lastNumberCard: '4844'},
    {name: 'Ян П.', lastNumberCard: '3324'},
  ];

  ngOnInit(): void {
  }

  onClickControl() {
    this.open = !this.open;
    this.expand = !this.expand;
  }
}
